function updatePrice() {
    let s = document.getElementsByName("prodType");
    let select = s[0];
    let price = 0;
    let prices = getPrices();
    let priceIndex = parseInt(select.value) - 1;
    let radioDiv = document.getElementById("radios");
    let radios = document.getElementsByName("prodOptions");
    let checkDiv = document.getElementById("checkboxes");
    let checkboxes = document.querySelectorAll("#checkboxes input");

    let f1 = document.getElementsByName("field1");
    f1[0].value = f1[0].value.replace(/[^0-9]/g, "");

    if (priceIndex >= 0) {
        price = prices.prodTypes[priceIndex];
    }

    if (select.value == "1" || select.value == "3") {
        radioDiv.style.display = "none";
    } else radioDiv.style.display = "block";

    radios.forEach(function(radio) {
        if (radio.checked) {
            let optionPrice = prices.prodOptions[radio.value];
            if (optionPrice != undefined) {
                price += optionPrice;
            }
        }
    });

    if (select.value == "1" || select.value == "2") {
        checkDiv.style.display = "none";
    } else checkDiv.style.display = "block";

    checkboxes.forEach(function(checkbox) {
        if (checkbox.checked) {
            let propPrice = prices.prodProperties[checkbox.name];
            if (propPrice !== undefined) {
                price += propPrice;
            }
        }
    });

    let prodPrice = document.getElementById("prodPrice");
    prodPrice.innerHTML = "Cумма: " + ((f1[0].value) * (price)) + " рублей";
}

function getPrices() {
    return {
        prodTypes: [1750000, 1700000, 1650000, 1600000],
        prodOptions: {
            option1: 125000,
            option2: 175000,
            option3: 355000,
        },
        prodProperties: {
            prop1: 125000,
            prop2: 69000,
            prop3: 26900,
        }
    };
}

window.addEventListener('DOMContentLoaded', function(event) {
    let radioDiv = document.getElementById("radios");
    radioDiv.style.display = "none";
    let s = document.getElementsByName("prodType");
    let select = s[0];
    select.addEventListener("change", function(event) {
        let target = event.target;
        console.log(target.value);
        updatePrice();
    });

    let radios = document.getElementsByName("prodOptions");
    radios.forEach(function(radio) {
        radio.addEventListener("change", function(event) {
            let r = event.target;
            console.log(r.value);
            updatePrice();
        });
    });

    let checkboxes = document.querySelectorAll("#checkboxes input");
    checkboxes.forEach(function(checkbox) {
        checkbox.addEventListener("change", function(event) {
            let c = event.target;
            console.log(c.name);
            console.log(c.value);
            updatePrice();
        });
    });

    updatePrice();
});